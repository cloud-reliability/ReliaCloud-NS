package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import controllers.MorphiaObject;
import play.Logger;
import play.data.validation.Constraints;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/13/13 Time: 1:21 PM
 */
@Entity
public class VMGroup {
  @Id
  private ObjectId id;

  @Constraints.Required
  private String name;

  @Reference
  private List<VM> vms = new ArrayList<>();

  private boolean isActive;
  private boolean isDefault; // specifies whether this is a global component
                             // that is available to all users

  @Reference
  private User user;

  public static void create(VMGroup group) {
    MorphiaObject.ds.save(group);
  }

  public static void delete(String idToDelete) {
    VMGroup toDelete = findById(idToDelete);

    if (toDelete != null) {
      Logger.info("toDelete: " + toDelete);
      MorphiaObject.ds.delete(toDelete);
    } else {
      Logger.debug("VMGroup id not Found: " + idToDelete);
    }
  }

  public static void update(VMGroup group) {
    MorphiaObject.ds.save(group);
  }

  public static void markInactive(String id) {
    VMGroup group = findById(id);

    if (group.isActive) {
      group.setActive(false);
      update(group);
    }
  }

  public static void markActive(String id) {
    VMGroup group = findById(id);

    if (!group.isActive) {
      group.setActive(true);
      update(group);
    }
  }

  public static List<VMGroup> findActive() {
    return MorphiaObject.ds.find(VMGroup.class).field("isActive").equal(true)
        .asList();
  }

  public static List<VMGroup> findHidden() {
    return MorphiaObject.ds.find(VMGroup.class).field("isActive").equal(false)
        .asList();
  }

  public static List<VMGroup> findAll() {
    return MorphiaObject.ds.find(VMGroup.class).asList();
  }

  public static VMGroup findById(String id) {
    return MorphiaObject.ds.find(VMGroup.class).field("_id")
        .equal(new ObjectId(id)).get();
  }

  public static List<VMGroup> findRefVMGroups(VM vm) {
    return MorphiaObject.ds.find(VMGroup.class).field("vms").hasThisElement(vm)
        .asList();
  }

  public static Map<String, String> findActiveMap() {
    HashMap<String, String> vmMap = new HashMap<>();
    for (VMGroup v : VMGroup.findActive()) {
      vmMap.put(String.valueOf(v.getId()), v.getName());
    }
    return vmMap;
  }

  public static Map<String, String> findAllMap() {
    HashMap<String, String> vmMap = new HashMap<>();
    for (VMGroup v : VMGroup.findAll()) {
      if (v.isActive())
        vmMap.put(String.valueOf(v.getId()), v.getName());
      else
        vmMap.put(String.valueOf(v.getId()), v.getName() + " (Hidden)");

    }
    return vmMap;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<VM> getVms() {
    return vms;
  }

  public void setVms(List<VM> vms) {
    this.vms = vms;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(boolean aDefault) {
    isDefault = aDefault;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}


