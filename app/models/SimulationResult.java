package models;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;

import controllers.Application;
import controllers.MorphiaObject;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/15/13 Time: 10:17 PM
 */

@Embedded
public class SimulationResult {
  private ObjectId id;

  private int pFails;
  private int mFails;
  private int hFails;
  private int bFails;

  private int totalFails;
  private int totalSamples;

  private double locp;
  private double sigma;

  private boolean isValid;
  private boolean isActive;

  private double runTime;

  private int pNeeded;
  private int mNeeded;
  private int hNeeded;
  private int bNeeded;

  @Embedded
  private ArrayList<VMCount> counts;

  @Embedded
  private List<Long> cpuAvail;

  @Embedded
  private List<Long> memAvail;

  @Embedded
  private List<Long> hddAvail;

  @Embedded
  private List<Long> bandAvail;

  @Embedded
  private List<Double> locpList;

  @Embedded
  private List<Double> vLocpList;

  @Embedded
  private List<Integer> numVmList;

  public SimulationResult() {}

  public SimulationResult(boolean valid, boolean active, double runTime,
      ArrayList<Integer> numVmList, ArrayList<Double> reliability,
      ArrayList<Double> stddev) {
    id = new ObjectId();
    isValid = valid;
    isActive = active;
    this.runTime = runTime;
    this.numVmList = numVmList;
    this.locpList = reliability;
    this.vLocpList = stddev;
  }
  public SimulationResult(int pFails, int mFails, int hFails, int bFails,
                          int totalFails, int totalSamples, double locp, double sigma,
                          boolean valid, boolean active, double runTime, int pNeeded, int mNeeded,
                          int hNeeded, int bNeeded, ArrayList<VMCount> counts, List<Long> cpuAvail,
                          List<Long> memAvail, List<Long> hddAvail, List<Long> bandAvail,
                          List<Double> locpList, List<Double> vLocpList) {

    id = new ObjectId();
    this.pFails = pFails;
    this.mFails = mFails;
    this.hFails = hFails;
    this.bFails = bFails;
    this.totalFails = totalFails;
    this.totalSamples = totalSamples;
    this.locp = locp;
    this.sigma = sigma;
    isValid = valid;
    isActive = active;
    this.runTime = runTime;
    this.pNeeded = pNeeded;
    this.mNeeded = mNeeded;
    this.hNeeded = hNeeded;
    this.bNeeded = bNeeded;
    this.counts = counts;
    this.cpuAvail = cpuAvail;
    this.memAvail = memAvail;
    this.hddAvail = hddAvail;
    this.bandAvail = bandAvail;
    this.locpList = locpList;
    this.vLocpList = vLocpList;
  }

  public SimulationResult(int totalFails, int totalSamples, double locp, double sigma,
                          boolean valid, boolean active, double runTime,List<Double> locpList,
                          List<Double> vLocpList) {

    id = new ObjectId();
    this.totalFails = totalFails;
    this.totalSamples = totalSamples;
    this.locp = locp;
    this.sigma = sigma;
    isValid = valid;
    isActive = active;
    this.runTime = runTime;
    this.locpList = locpList;
    this.vLocpList = vLocpList;
  }

  public static List<SimulationResult> page(int page, int pageSize,
      String sortBy, String order, String filter) {
    String userId = Application.getUserId();

    return MorphiaObject.ds.createQuery(SimulationResult.class).limit(pageSize)
        .offset(page).order(order).asList();
  }

  public static List<SimulationResult> pageBySim(String simId, int page,
      int pageSize, String sortBy, String order, String filter) {
    User user = User.findCurrentUser();

    return MorphiaObject.ds.find(Simulation.class).field("_id")
        .equal(new ObjectId(simId)).field("user").equal(user)
        .retrievedFields(true, "results.locp", "results.totalSamples",
            "results.totalFails", "results.runTime")
        .limit(pageSize).get().getResults();
  }

  public ObjectId getId() {
    return id;
  }

  public int getpFails() {
    return pFails;
  }

  public int getmFails() {
    return mFails;
  }

  public int gethFails() {
    return hFails;
  }

  public int getbFails() {
    return bFails;
  }

  public double getLOCP() {
    return locp;
  }

  public int getTotalFails() {
    return totalFails;
  }

  public int getTotalSamples() {
    return totalSamples;
  }

  public double getSigma() {
    return sigma;
  }

  public boolean isValid() {
    return isValid;
  }

  public boolean isActive() {
    return isActive;
  }

  public double getRunTime() {
    return runTime;
  }

  public int getpNeeded() {
    return pNeeded;
  }

  public int getmNeeded() {
    return mNeeded;
  }

  public int gethNeeded() {
    return hNeeded;
  }

  public int getbNeeded() {
    return bNeeded;
  }

  public ArrayList<VMCount> getCounts() {
    return counts;
  }

  public void setCounts(ArrayList<VMCount> counts) {
    this.counts = counts;
  }

  public List<Long> getCpuAvail() {
    return cpuAvail;
  }

  public List<Long> getMemAvail() {
    return memAvail;
  }

  public List<Long> getHddAvail() {
    return hddAvail;
  }

  public List<Long> getBandAvail() {
    return bandAvail;
  }

  public List<Double> getLocpList() {
    return locpList;
  }

  public List<Double> getvLocpList() {
    return vLocpList;
  }

  public double getLocp() {
    return locp;
  }

  public void setLocp(double locp) {
    this.locp = locp;
  }

  public List<Integer> getNumVmList() {
    return numVmList;
  }

  public void setNumVmList(List<Integer> numVmList) {
    this.numVmList = numVmList;
  }

  @Override
  public String toString() {
    return "SimulationResult{" + "pFails=" + pFails + ", mFails=" + mFails
        + ", hFails=" + hFails + ", bFails=" + bFails + ", totalFails="
        + totalFails + ", totalSamples=" + totalSamples + ", locp=" + locp
        + ", sigma=" + sigma + ", runTime=" + runTime + ", pNeeded=" + pNeeded
        + ", mNeeded=" + mNeeded + ", hNeeded=" + hNeeded + ", bNeeded="
        + bNeeded + ", counts=" + counts + '}';
  }
}
