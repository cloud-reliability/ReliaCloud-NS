package models;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import controllers.MorphiaObject;
import play.Logger;
import play.data.validation.Constraints;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 8:35 AM
 */
@Entity
public class VM {

  @Id
  private ObjectId id;

  @Constraints.Required
  private String name;

  @Constraints.Required
  private int cores;

  @Constraints.Required
  private int memory;

  @Constraints.Required
  private int hdd;

  @Constraints.Required
  private int bandwidth;

  private Cluster cluster;

  private  int requiredNodes;


  private boolean isActive;
  private boolean isDefault; // specifies whether this is a global component
  // that is available to all users

  @Reference
  private User user;

  private int allocatedCores;
  private int allocatedMem;
  private int allocatedBW;
  private int allocatedHdd;

  public static void create(VM vm) {
    MorphiaObject.ds.save(vm);
  }

  public static void delete(String idToDelete) {
    VM toDelete = findById(idToDelete);

    if (toDelete != null) {
      Logger.info("toDelete: " + toDelete);
      MorphiaObject.ds.delete(toDelete);
    } else {
      Logger.debug("VM id not Found: " + idToDelete);
    }
  }

  public static void update(VM vm) {
    MorphiaObject.ds.save(vm);
  }

  public static void markInactive(String id) {
    VM vm = findById(id);
    if (vm.isActive) {
      vm.setActive(false);
      update(vm);
    }
  }

  public static void markActive(String id) {
    VM vm = findById(id);
    if (!vm.isActive) {
      vm.setActive(true);
      update(vm);
    }
  }

  public static List<VM> findActive() {
    return MorphiaObject.ds.find(VM.class).field("isActive").equal(true)
            .asList();
  }

  public static List<VM> findHidden() {
    return MorphiaObject.ds.find(VM.class).field("isActive").equal(false)
            .asList();
  }

  public static VM findById(String id) {
    return MorphiaObject.ds.find(VM.class).field("_id").equal(new ObjectId(id))
            .get();
  }

  @Override
  public String toString() {
    return "VM{" + "idVm=" + id + ", name='" + name + '\'' + ", cores=" + cores
            + ", memory=" + memory + ", hddSize=" + hdd + ", bandwidth=" + bandwidth
            + '}';
  }

  public static boolean hasActiveGroupRef(VM vm) {
    List<VMGroup> groups = VMGroup.findRefVMGroups(vm);

    boolean hasActiveRef = false;
    for (VMGroup group : groups) {
      if (group.isActive()) {
        hasActiveRef = true;
        break;
      }
    }

    return hasActiveRef;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCores() {
    return cores;
  }

  public void setCores(int cores) {
    this.cores = cores;
  }

  public int getMemory() {
    return memory;
  }

  public void setMemory(int memory) {
    this.memory = memory;
  }

  public int getHdd() {
    return hdd;
  }

  public void setHdd(int hdd) {
    this.hdd = hdd;
  }

  public int getBandwidth() {
    return bandwidth;
  }

  public void setBandwidth(int bandwidth) {
    this.bandwidth = bandwidth;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(boolean aDefault) {
    isDefault = aDefault;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    VM vm = (VM) o;

    return id.equals(vm.id);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }



  public Cluster getCluster() {
    return cluster;
  }

  public void setCluster(Cluster cluster) {
    this.cluster = cluster;
  }

  public int getRequiredNodes() {
    return requiredNodes;
  }

  public void setRequiredNodes(int requiredNodes) {
    this.requiredNodes = requiredNodes;
  }

  public int getAllocatedCores() {
    return allocatedCores;
  }

  public void setAllocatedCores(int allocatedCores) {
    this.allocatedCores = allocatedCores;
  }

  public int getAllocatedMem() {
    return allocatedMem;
  }

  public void setAllocatedMem(int allocatedMem) {
    this.allocatedMem = allocatedMem;
  }

  public int getAllocatedBW() {
    return allocatedBW;
  }

  public void setAllocatedBW(int allocatedBW) {
    this.allocatedBW = allocatedBW;
  }

  public int getAllocatedHdd() {
    return allocatedHdd;
  }

  public void setAllocatedHdd(int allocatedHdd) {
    this.allocatedHdd = allocatedHdd;
  }
}
