package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import controllers.MorphiaObject;
import play.Logger;
import play.data.validation.Constraints;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 8:35 AM
 */

@Entity
public class Cloud {

  @Id
  private ObjectId id;

  @Constraints.Required
  private String name;

  @Reference
  private List<Cluster> clusters = new ArrayList<>();

  private boolean isActive;
  private boolean isDefault; // specifies whether this is a global component
  // that is available to all users

  @Reference
  private User user;

  public static void create(Cloud cloud) {
    MorphiaObject.ds.save(cloud);
  }

  public static void delete(String idToDelete) {
    Cloud toDelete = findById(idToDelete);

    if (toDelete != null) {
      Logger.info("toDelete: " + toDelete);
      MorphiaObject.ds.delete(toDelete);
    } else {
      Logger.debug("Cloud id not Found: " + idToDelete);
    }
  }

  public static void update(Cloud cloud) {
    MorphiaObject.ds.save(cloud);
  }

  public static void markInactive(String id) {
    Cloud cloud = findById(id);

    if (cloud.isActive) {
      cloud.setActive(false);
      update(cloud);
    }
  }

  public static void markActive(String id) {
    Cloud cloud = findById(id);

    if (!cloud.isActive) {
      cloud.setActive(true);
      update(cloud);
    }
  }

  public static List<Cloud> findActive() {
    return MorphiaObject.ds.find(Cloud.class).field("isActive").equal(true)
            .asList();
  }

  public static List<Cloud> findHidden() {
    return MorphiaObject.ds.find(Cloud.class).field("isActive").equal(false)
            .asList();
  }

  public static List<Cloud> findAll() {
    return MorphiaObject.ds.find(Cloud.class).asList();
  }

  public static Cloud findById(String id) {
    return MorphiaObject.ds.find(Cloud.class).field("_id")
            .equal(new ObjectId(id)).get();
  }

  public static List<Cloud> findRefClouds(Cluster c) {
    return MorphiaObject.ds.find(Cloud.class).field("clusters")
            .hasThisElement(c).asList();
  }

  public static Map<String, String> findActiveMap() {
    HashMap<String, String> clustMap = new HashMap<>();

    for (Cloud c : Cloud.findActive())
      clustMap.put(String.valueOf(c.getId()), c.getName());

    return clustMap;
  }

  public static Map<String, String> findAllMap() {
    HashMap<String, String> clustMap = new HashMap<>();

    for (Cloud c : Cloud.findAll()) {
      if (c.isActive())
        clustMap.put(String.valueOf(c.getId()), c.getName());
      else
        clustMap.put(String.valueOf(c.getId()), c.getName() + " (Hidden)");

    }

    return clustMap;
  }

  @Override
  public String toString() {
    return "Cloud{" + "id=" + id + ", name='" + name + '\'' + ", clusters="
            + clusters + ", isActive=" + isActive + '}';
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Cluster> getClusters() {
    return clusters;
  }

  public void setClusters(List<Cluster> clusters) {
    this.clusters = clusters;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(boolean aDefault) {
    isDefault = aDefault;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }



  public static int getNumberOfNodes(ArrayList<Cluster> clusterList) {
    int totalNodes = 0;
    for(Cluster c : clusterList)
      totalNodes+= c.getNodes();

    return totalNodes;
  }







/*  public Cluster moreFreeCluster(){

    int max=0;

    for(Cluster c : clusters){
      max = c.getAvaliCores();

    }




  }
*/
}

