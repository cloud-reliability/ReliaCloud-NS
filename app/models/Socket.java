package models;

import java.util.HashMap;
import java.util.Map;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import play.Logger;
import play.libs.Akka;
import play.libs.F;
import play.mvc.WebSocket;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/18/13 Time: 10:48 AM
 */
public class Socket extends UntypedActor {

  private static ActorRef actor =
      Akka.system().actorOf(new Props(Socket.class));

  private Map<String, WebSocket.Out<String>> registrered = new HashMap<>();

  public static void register(final String id, final WebSocket.In<String> in,
      final WebSocket.Out<String> out) throws Exception {

    actor.tell(new Registration(id, out), actor);

    // For each event received on the socket,
    in.onMessage(new F.Callback<String>() {
      @Override
      public void invoke(String event) {
        // nothing to do
      }
    });

    // When the socket is closed.
    in.onClose(new F.Callback0() {
      @Override
      public void invoke() {
        actor.tell(new UnregistrationMessage(id), actor);
      }
    });
  }

  public static void update(String id) {
    actor.tell(new ResultReady(id), actor);
  }

  @Override
  public void onReceive(Object message) throws Exception {

    if (message instanceof Registration) {

      Registration registration = (Registration) message;

      Logger.info("Registering " + registration.id + "...");
      registrered.put(registration.id, registration.channel);
    } else if (message instanceof ResultReady) {

      // Received a Move message
      ResultReady resultReady = (ResultReady) message;

      for (WebSocket.Out<String> channel : registrered.values()) {
        channel.write(resultReady.id);
      }
    } else if (message instanceof UnregistrationMessage) {
      // Received a Unregistration message
      UnregistrationMessage quit = (UnregistrationMessage) message;

      Logger.info("Unregistering " + quit.id + "...");
      registrered.remove(quit.id);
    } else {
      unhandled(message);
    }

  }

  public static class Registration {
    public String id;
    public WebSocket.Out<String> channel;

    public Registration(String id, WebSocket.Out<String> channel) {
      super();
      this.id = id;
      this.channel = channel;
    }
  }

  public static class UnregistrationMessage {
    public String id;

    public UnregistrationMessage(String id) {
      super();
      this.id = id;
    }
  }

  public static class ResultReady {
    public String id;

    public ResultReady(String id) {
      this.id = id;
    }
  }
}
