package controllers;

import models.Socket;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import views.html.home;

public class Application extends Controller {

  @SecureSocial.SecuredAction
  public static Result index() {
    return redirect(securesocial.controllers.routes.LoginPage.login());
  }

  @SecureSocial.SecuredAction
  public static Result pushSimNotification(String simResId) {
    Socket.update("Simulation result " + simResId + " is now available.");
    return ok();
  }

  @SecureSocial.SecuredAction
  public static WebSocket<String> socketHandler() {
    return new WebSocket<String>() {
      @Override
      public void onReady(In<String> in, Out<String> out) {
        try {
          Socket.register(java.util.UUID.randomUUID().toString(), in, out);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    };
  }

  @SecureSocial.UserAwareAction
  public static String getUsersFullName() {
    Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
    return user != null ? user.fullName() : "guest";
  }

  @SecureSocial.UserAwareAction
  public static String getUserId() {
    Identity user = (Identity) ctx().args.get(SecureSocial.USER_KEY);
    return user != null ? user.identityId().userId() : null;
  }

  @SecureSocial.SecuredAction
  public static Result goHome() {
    return ok(home.render());
  }
}
