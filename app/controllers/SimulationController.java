package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import models.Cloud;
import models.Cluster;
import models.Simulation;
import models.User;
import models.VM;
import models.VMGroup;
import models.VMProbabilityMap;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import simulation.SimUtils;
import simulation.actors.MasterActor;
import simulation.actors.ReliabilityCurveMasterActor;
import simulation.data.AllocationData;
import views.html.simDistTables;
import views.html.simList;
import views.html.simOverviewTable;
import views.html.simSetupForm;
import views.html.simTable;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 4:47 PM
 */

public class SimulationController extends Controller {
  private final static Form<Simulation> simulateForm = form(Simulation.class);
  private final static String DESIRED_AVAIL = "0.8"; // default desired
  // availability

  @SecureSocial.SecuredAction
  public static Result simSetup() {
    HashMap<String, String> data = new HashMap<>();
    data.put("simType", "Normal"); // set default simType to normal
    data.put("distributionType", "Identical"); // set default allocation to identical
    data.put("convergenceType", "Full");
    data.put("desAvail", DESIRED_AVAIL);
    data.put("oversubType", "Normal");
    data.put("allocationType", "Original");

    final Form<Simulation> simForm = form(Simulation.class).bind(data);
    return ok(simSetupForm.render(simForm, VMGroup.findActiveMap(),
            Cloud.findActiveMap(), null));
  }

  @SecureSocial.SecuredAction
  public static Result simSetupDuplicate(String simId) {
    Simulation sim = Simulation.findById(simId);

    HashMap<String, String> data = new HashMap<>();

    data.put("name", sim.getName());
    data.put("numVMs", String.valueOf(sim.getNumVMs()));
    data.put("numSims", String.valueOf(sim.getNumSims()));
    data.put("vmgroup", String.valueOf(sim.getVmGroup().getId()));
    data.put("cloudid", String.valueOf(sim.getCloud().getId()));
    data.put("simType", sim.getSimType());
    data.put("distributionType", sim.getVmDistributionType());
    data.put("desAvail", String.valueOf(sim.getDesiredAvail()));
    data.put("convergenceType", sim.getConvergenceType());
    data.put("oversubType", sim.getOversubType());
    data.put("allocationPolicy", sim.getVmAllocationType());



    final Form<Simulation> simForm = form(Simulation.class).bind(data);

    if (!sim.getVmGroup().isActive() || !sim.getCloud().isActive())
      return ok(simSetupForm.render(simForm, VMGroup.findAllMap(),
              Cloud.findAllMap(), simId));
    else
      return ok(simSetupForm.render(simForm, VMGroup.findActiveMap(),
              Cloud.findActiveMap(), simId));
  }

  @SecureSocial.SecuredAction
  public static Result save() {
      double desiredAvail = 0;
    Form<Simulation> simulateForm = form(Simulation.class).bindFromRequest();

    if (simulateForm.hasErrors()) {
      System.out.println("bad request" + simulateForm.errors());
      return badRequest(simSetupForm.render(simulateForm,
              VMGroup.findActiveMap(), Cloud.findActiveMap(), null));
    }

    String vmGroupId        = simulateForm.bindFromRequest().data().get("vmgroup");
    String cloudId          = simulateForm.bindFromRequest().data().get("cloudid");
    int numSims             = Integer.valueOf(simulateForm.bindFromRequest().data().get("numSims"));
    //desiredAvail     = Double.valueOf(simulateForm.bindFromRequest().data().get("desAvail"));
    String distributionType = simulateForm.bindFromRequest().data().get("distributionType");
    String allocationType   = simulateForm.bindFromRequest().data().get("allocationType");

    if(simulateForm.bindFromRequest().data().get("desiredAvail") == null){
        desiredAvail = 0.9;
    }

    if (numSims <= 0) {
      numSims = 1;
    }

    simulateForm.get().setNumSims(numSims);
    simulateForm.get().setDesiredAvail(desiredAvail);
    simulateForm.get().setVmAllocationType(allocationType);
    simulateForm.get().setVmDistributionType(distributionType);

    simulateForm.get().setVmGroup(VMGroup.findById(vmGroupId));
    simulateForm.get().setProbabilityList(generateProbMaps(simulateForm.get()));
    simulateForm.get().setCloud(Cloud.findById(cloudId));
    simulateForm.get().setActive(true);
    simulateForm.get().setValid(true);
    simulateForm.get().setUser(User.findCurrentUser());
    simulateForm.get().setDate(new java.util.Date());
    Simulation.create(simulateForm.get());

    flash("success",
            "Simulation " + simulateForm.get().getName() + " has been created");

    startSim(simulateForm.get().getId().toString());

    return redirect(routes.SimulationController.list("id", "desc", ""));
  }

  private static void startSim(final String simId) {
    final Simulation sim = Simulation.findById(simId);

    ActorSystem system = ActorSystem.create("mysys");
    ActorRef master;

    switch (sim.getSimType()) {
      case "Normal":
        master = system.actorOf(new Props(new UntypedActorFactory() {
          public UntypedActor create() {
            return new MasterActor(sim.getId().toString(), sim.getNumSims());
          }
        }), "master" + simId);

        master.tell(new AllocationData(sim),
                system.actorFor("/user/master" + simId));
        break;

      case "Reliability":

        sim.setNumVMs(SimUtils.calculateVMsForFail(sim));

        master = system.actorOf(new Props(new UntypedActorFactory() {
          public UntypedActor create() {
            return new ReliabilityCurveMasterActor(simId);
          }
        }), "master" + simId);
        master.tell("simulate", master); // send start message to master
        break;
      default:
        System.out.println("Unrecognized simulation type.");
    }
  }

  private static ArrayList<VMProbabilityMap> generateProbMaps(Simulation sim) {
    List<VM> vms = sim.getVmGroup().getVms();
    ArrayList<VMProbabilityMap> probMap = new ArrayList<>();

    // iterate through each VM in group and add an entry to the probability map
    for (final VM vm : vms) {
      double prob = Double.valueOf(simulateForm.bindFromRequest().data()
              .get("prob" + vm.getId().toString()));

      if (prob < 0)
        prob = 0;

      probMap.add(new VMProbabilityMap(vm, prob));
    }
    return probMap;
  }

  @SecureSocial.SecuredAction(ajaxCall = true)
  public static Result getTables(String vmGroupId, String cloudId) {
    VMGroup vmGroup = VMGroup.findById(vmGroupId);
    Cloud cloud = Cloud.findById(cloudId);

    try {
      HashMap<String, Long> cloudResources = calculateCloudResources(cloud);
      return ok(
              simDistTables.render(vmGroup, cloud, null, null, cloudResources));
    } catch (NumberFormatException nfe) {
      System.out.println(
              "SimulationController.getTables: Caught number format exception while trying to load tables via Ajax.");
    }

    return ok();
  }

  @SecureSocial.SecuredAction(ajaxCall = true)
  public static Result getTablesAndDist(String simId) {
    Simulation sim = Simulation.findById(simId);
    try {
      HashMap<String, Long> cloudResources =
              calculateCloudResources(sim.getCloud());

      return ok(simDistTables.render(sim.getVmGroup(), sim.getCloud(), simId,
              sim.getProbabilityMap(), cloudResources));
    } catch (NumberFormatException nfe) {
      System.out.println(
              "SimulationController.getTablesAndDist: Caught number format exception while trying to load tables via Ajax.");
    }

    return ok();
  }

  @SecureSocial.SecuredAction(ajaxCall = true)
  public static Result getOverview() {
    Map<String, String[]> params = request().queryString();

    String cloudId = null;
    String vmGroupId = null;
    Long numVMs = (long) 0;

    HashMap<String, Double> probs = new HashMap<>();
    for (String key : params.keySet()) {
      if (key.startsWith("prob")) {
        for (String s : params.get(key)) {
          String vmId = key.substring(4);
          probs.put(vmId, Double.valueOf(s));
        }
      } else if (key.equals("cloudid")) {
        cloudId = params.get(key)[0];
      } else if (key.equals("vmgroupid")) {
        vmGroupId = params.get(key)[0];
      } else if (key.equals("numVMs")) {
        numVMs = Long.valueOf(params.get(key)[0]);
      } else {
        System.out.println(
                "SimulationController.getOverview: parameter not recognized.");
      }
    }

    HashMap<String, Long> vmRes =
            calculateVMGroupResources(vmGroupId, numVMs, probs);
    HashMap<String, Long> cloudRes =
            calculateCloudResources(Cloud.findById(cloudId));

    return ok(simOverviewTable.render(vmGroupId, cloudId, cloudRes, vmRes));
  }

  private static HashMap<String, Long> calculateCloudResources(Cloud c) {
    // calculate total available server resources
    long cpu = 0;
    long mem = 0;
    long hdd = 0;
    long band = 0;
    HashMap<String, Long> totalServer = new HashMap<>();

    for (Cluster cluster : c.getClusters()) {
      long nodes = cluster.getNodes();
      cpu += cluster.getCores() * nodes;
      mem += cluster.getMemory() * nodes;
      hdd += cluster.getHdd() * nodes;
      band += cluster.getBandwidth() * nodes;
    }
    totalServer.put("cpu", cpu);
    totalServer.put("mem", mem);
    totalServer.put("hdd", hdd);
    totalServer.put("band", band);

    return totalServer;
  }

  private static HashMap<String, Long> calculateVMGroupResources(
          String vmGroupId, Long numVMs, HashMap<String, Double> probs) {
    // calculate total available VM resources
    long cpu = 0;
    long mem = 0;
    long hdd = 0;
    long band = 0;

    HashMap<String, Long> totalVM = new HashMap<>();
    VMGroup vg = VMGroup.findById(vmGroupId);

    if (probs.size() <= 0) { // if no probabilities were mapped return all 0
      totalVM.put("cpu", cpu);
      totalVM.put("mem", mem);
      totalVM.put("hdd", hdd);
      totalVM.put("band", band);

      return totalVM;
    }

    for (VM vm : vg.getVms()) {
      double prob = 0;
      if (probs.containsKey(vm.getId().toString()))
        prob = probs.get(vm.getId().toString());

      double count = prob * numVMs;
      cpu += vm.getCores() * count;
      mem += vm.getMemory() * count;
      hdd += vm.getHdd() * count;
      band += vm.getBandwidth() * count;
    }

    totalVM.put("cpu", cpu);
    totalVM.put("mem", mem);
    totalVM.put("hdd", hdd);
    totalVM.put("band", band);
    return totalVM;
  }

  @SecureSocial.SecuredAction
  public static Result list(String sortBy, String order, String filter) {
    return ok(simList.render(Simulation.page(new java.util.Date().toString(),
            20, sortBy, order, filter), sortBy, order, filter));
  }

  /**
   * Display the paginated list of results
   *
   * @param sortBy Column to be sorted
   * @param order Sort order (either asc or desc)
   */
  @SecureSocial.SecuredAction(ajaxCall = true)
  public static Result getTable(String date, String sortBy, String order,
                                String filter) {
    return ok(simTable.render(Simulation.page(date, 20, sortBy, order, filter),
            sortBy, order, filter));
  }
}

