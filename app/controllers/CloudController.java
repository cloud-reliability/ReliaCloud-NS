package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import models.Cloud;
import models.Cluster;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import views.html.cloudCreateForm;
import views.html.cloudEditForm;
import views.html.cloudList;
import views.html.cloudViewForm;

/**
 * User: Brett Date: 1/11/13 Time: 9:10 AM
 */

public class CloudController extends Controller {
  private final static Form<Cloud> cloudForm = form(Cloud.class);

  @SecureSocial.SecuredAction
  public static Result create() {
    return ok(cloudCreateForm.render(cloudForm, Cluster.findActive()));
  }

  /**
   * Handle the 'new Cloud form' submission
   */
  @SecureSocial.SecuredAction
  public static Result save() {
    Form<Cloud> cloudForm = form(Cloud.class).bindFromRequest();
    if (cloudForm.hasErrors()) {
      return badRequest(
          cloudCreateForm.render(cloudForm, Cluster.findActive()));
    }

    Set<String> keys = cloudForm.bindFromRequest().data().keySet();

    cloudForm.get().setClusters(generateClusterList(keys)); // pull in the newly
                                                            // added VMs
    cloudForm.get().setActive(true); // on creation set active to true
    cloudForm.get().setDefault(false); // this VM is tied to a specific user
    cloudForm.get().setUser(User.findCurrentUser());
    Cloud.create(cloudForm.get());

    flash("success",
        "Cluster " + cloudForm.get().getName() + " has been created");
    return redirect(routes.CloudController.list());
  }

  @SecureSocial.SecuredAction
  public static Result list() {
    return ok(cloudList.render(Cloud.findActive(), Cloud.findHidden()));
  }

  /**
   * Display the 'view form' of a existing Cloud.
   * 
   * @param id Id of the Cloud to view
   */
  @SecureSocial.SecuredAction
  public static Result view(String id) {
    return ok(cloudViewForm.render(Cloud.findById(id)));
  }

  /**
   * Display the 'edit form' of a existing Cloud.
   * 
   * @param id Id of the Cluster to edit
   */
  @SecureSocial.SecuredAction
  public static Result edit(String id) {
    Form<Cloud> editCloudForm = form(Cloud.class).fill(Cloud.findById(id));
    return ok(cloudEditForm.render(editCloudForm, Cloud.findById(id),
        Cluster.findActive()));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id Id of the Cloud to edit
   */
  @SecureSocial.SecuredAction
  public static Result update(String id) {
    Form<Cloud> editCloudForm = form(Cloud.class).bindFromRequest();

    if (editCloudForm.hasErrors()) {
      return badRequest(cloudViewForm.render(Cloud.findById(id)));
    }

    // retrieve request keys in order to determine which VMs were selected for
    // the group
    Set<String> keys = editCloudForm.bindFromRequest().data().keySet();

    editCloudForm.get().setClusters(generateClusterList(keys)); // pull in the
                                                                // newly added
                                                                // VMs
    editCloudForm.get().setId(new ObjectId()); // create new id as we are making
                                               // copy actually
    editCloudForm.get().setActive(true); // renew that this is still active on
                                         // updates
    Cloud.update(editCloudForm.get());

    flash("success",
        "VM Group" + editCloudForm.get().getName() + " has been updated");
    return redirect(routes.CloudController.list());
  }

  /**
   * Handle Cluster deletion
   */
  @SecureSocial.SecuredAction
  public static Result delete(String id) {
    Cloud.delete(id);
    flash("success", "Cloud has been deleted");
    return redirect(routes.CloudController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markInactive(String id) {
    Cloud.markInactive(id);
    return redirect(routes.CloudController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markActive(String id) {
    Cloud.markActive(id);
    return redirect(routes.CloudController.list());
  }

  private static List<Cluster> generateClusterList(Set<String> keys) {
    List<Cluster> clusters = new ArrayList<>();
    for (String key : keys) {
      if (!(key.equals("name") || key.equals("id"))) {
        try {
          clusters.add(Cluster.findById(key));
        } catch (Exception e) {
          System.out.println(e.toString());
        }
      }
    }

    return clusters;
  }
}
