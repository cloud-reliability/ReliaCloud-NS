package controllers;

import static play.data.Form.form;

import org.bson.types.ObjectId;

import models.Cluster;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import views.html.clusterCreateForm;
import views.html.clusterEditForm;
import views.html.clusterList;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 4:59 PM
 */

public class ClusterController extends Controller {
  private final static Form<Cluster> clustForm = form(Cluster.class);

  @SecureSocial.SecuredAction
  public static Result create() {
    return ok(clusterCreateForm.render(clustForm));
  }

  /**
   * Handle the 'new Cluster form' submission
   */
  @SecureSocial.SecuredAction
  public static Result save() {
    Form<Cluster> clustForm = form(Cluster.class).bindFromRequest();

    if (clustForm.hasErrors()) {
      return badRequest(clusterCreateForm.render(clustForm));
    }
    clustForm.get().setActive(true); // on creation set active
    clustForm.get().setDefault(false); // this VM is tied to a specific user
    clustForm.get().setUser(User.findCurrentUser());
    Cluster.create(clustForm.get());

    flash("success",
        "Cluster " + clustForm.get().getName() + " has been created");
    return redirect(routes.ClusterController.list());
  }

  /**
   * Handle Cluster deletion
   */
  @SecureSocial.SecuredAction
  public static Result delete(String id) {
    Cluster.delete(id);
    flash("success", "Cluster has been deleted");
    return redirect(routes.ClusterController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markInactive(String id) {
    Cluster.markInactive(id);
    return redirect(routes.ClusterController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markActive(String id) {
    Cluster.markActive(id);
    return redirect(routes.ClusterController.list());
  }

  /**
   * Display the 'edit form' of a existing Cluster.
   * 
   * @param id Id of the Cluster to edit
   */
  @SecureSocial.SecuredAction
  public static Result edit(String id) {
    Form<Cluster> clustForm = form(Cluster.class).fill(Cluster.findById(id));

    return ok(clusterEditForm.render(id, clustForm));
  }

  /**
   * Handle the 'update form' submission
   * 
   * @param id Id of the Cluster to edit
   */
  @SecureSocial.SecuredAction
  public static Result update(String id) {
    Form<Cluster> clustForm = form(Cluster.class).bindFromRequest();
    if (clustForm.hasErrors()) {
      return badRequest(clusterEditForm.render(id, clustForm));
    }

    clustForm.get().setId(new ObjectId());
    clustForm.get().setActive(true);
    clustForm.get().setDefault(false);
    Cluster.update(clustForm.get());

    flash("success",
        "Cluster " + clustForm.get().getName() + " has been updated");
    return redirect(routes.ClusterController.list());
  }

  @SecureSocial.SecuredAction
  public static Result list() {
    return ok(clusterList.render(Cluster.findActive(), Cluster.findHidden()));
  }
}

