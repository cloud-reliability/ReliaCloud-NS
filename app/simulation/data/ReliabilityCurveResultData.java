package simulation.data;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/19/13 Time: 8:43 PM To change
 * this template use File | Settings | File Templates.
 */
public final class ReliabilityCurveResultData {
  private final int numVMs;
  private final double availability;

  public ReliabilityCurveResultData(int numVMs, double availability) {
    this.numVMs = numVMs;
    this.availability = availability;
  }

  public int getNumVMs() {
    return numVMs;
  }

  public double getAvailability() {
    return availability;
  }
}
