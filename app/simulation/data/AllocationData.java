package simulation.data;

import models.Simulation;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/14/13 Time: 11:38 PM
 */
public final class AllocationData {
  private final Simulation sim;

  public AllocationData(Simulation sim) {
    this.sim = sim;
  }

  public Simulation getSim() {
    return sim;
  }
}
