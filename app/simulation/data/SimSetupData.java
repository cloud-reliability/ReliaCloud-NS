package simulation.data;

import java.util.ArrayList;
import java.util.List;

import models.Cluster;
import models.Simulation;
import models.VM;
import models.VMCount;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/14/13 Time: 11:22 PM
 */
public final class SimSetupData {
  private final Simulation sim;
  private final String vmDistributionType;
  private final String vmAllocationType;
  private ArrayList<VM> vms;
  private final ArrayList<VMCount> counts;

  private final int pNeeded;
  private final int mNeeded;
  private final int hNeeded;
  private final int bNeeded;

  public SimSetupData(Simulation sim, ArrayList<VM> vms ,ArrayList<VMCount> counts, int pNeeded,
                      int mNeeded, int hNeeded, int bNeeded) {
    this.sim = sim;
    this.counts = counts;
    this.vmAllocationType = sim.getVmAllocationType();
    this.vmDistributionType = sim.getVmDistributionType();
    this.vms = vms;
    this.pNeeded = pNeeded;
    this.mNeeded = mNeeded;
    this.hNeeded = hNeeded;
    this.bNeeded = bNeeded;
  }

  public Simulation getSim() {
    return sim;
  }

  public String getVmAllocationType() {
    return vmAllocationType;
  }
  public String getVmDistributionType() {
    return vmDistributionType;
  }

  public ArrayList<VMCount> getCounts() {
    return counts;
  }

  public int getpNeeded() {
    return pNeeded;
  }

  public int getmNeeded() {
    return mNeeded;
  }

  public int gethNeeded() { return hNeeded; }

  public int getbNeeded() {
    return bNeeded;
  }

  public ArrayList<VM> getVms() {
    return vms;
  }

  public void setVms(ArrayList<VM> vms) {
    this.vms = vms;
  }

}
