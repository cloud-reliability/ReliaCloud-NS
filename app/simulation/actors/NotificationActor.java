package simulation.actors;

import akka.actor.UntypedActor;
import controllers.Application;
import simulation.data.NotificationData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/15/13 Time: 10:48 AM
 */
class NotificationActor extends UntypedActor {
  private int numCompleted = 0;
  private final int numSims;

  public NotificationActor(int numSims) {
    this.numSims = numSims;
  }

  @Override
  public void preStart() {
    System.out.println("Notify actor started");
  }

  @Override
  public void postStop() {
    System.out.println("Notify actor desroyed");
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof NotificationData) {
      NotificationData nd = (NotificationData) message;

      // if all simulations are complete send kill signal to master
      if (++numCompleted == numSims)
        sender().tell("stop", getSelf());

      // push notification via Websocket
      Application.pushSimNotification(nd.getSimResId());

    } else {
      unhandled(message);
    }
  }
}
