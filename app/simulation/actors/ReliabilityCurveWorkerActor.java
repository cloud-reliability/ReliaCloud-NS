package simulation.actors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import akka.actor.UntypedActor;
import models.Cluster;
import models.Simulation;
import models.SimulationResult;
import play.db.DB;
import simulation.data.ReliabilityCurveResultData;
import simulation.data.SimSetupData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/12/13 Time: 11:36 PM
 */
class ReliabilityCurveWorkerActor extends UntypedActor {
  @Override
  public void postStop() {
    System.out.println("Child Killed");
  }

  @Override
  public void preStart() {
    System.out.println("Child ReliabilityCurveWorkerActor Created");
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof SimSetupData) {
      sender().tell(simulate((SimSetupData) message), getSelf()); // send result
      // back to
      // master
    } else
      unhandled(message);
  }

  private ReliabilityCurveResultData simulate(SimSetupData ssd)
          throws SQLException {
    double locp;
    double sigma;
    int totalFails = 0;
    int totalSamples = 0;
    double vLocp;

    List<Long> cpuAvail = new ArrayList<>(2000000);
    List<Long> memAvail = new ArrayList<>(2000000);
    List<Long> hddAvail = new ArrayList<>(2000000);
    List<Long> bandAvail = new ArrayList<>(2000000);

    List<Double> locsList = new ArrayList<>(2000000);
    List<Double> vLocsList = new ArrayList<>(2000000);

    long hAvail;
    long pAvail;
    long bAvail;
    long mAvail;

    int pNeeded = ssd.getpNeeded();
    int mNeeded = ssd.getmNeeded();
    int hNeeded = ssd.gethNeeded();
    int bNeeded = ssd.getbNeeded();

    // retrieve simulation for saving SimulationResult
    final Simulation sim = ssd.getSim();
    while (true) {
      hAvail = 0;
      pAvail = 0;
      bAvail = 0;
      mAvail = 0;

      boolean[] currState;
      for (final Cluster cluster : sim.getCloud().getClusters()) {
        for (int j = 0; j < cluster.getNodes(); j++) {
          currState = cluster.getState();

          if (currState[0])
            pAvail += cluster.getCores();
          if (currState[1])
            mAvail += cluster.getMemory();
          if (currState[2])
            hAvail += cluster.getHdd();
          if (currState[3])
            bAvail += cluster.getBandwidth();
        }
      }

      cpuAvail.add(pAvail);
      memAvail.add(mAvail);
      hddAvail.add(hAvail);
      bandAvail.add(bAvail);

      if (!(pAvail >= pNeeded && mAvail >= mNeeded && hAvail >= hNeeded
              && bAvail >= bNeeded)) {

        totalFails++;
      }
      totalSamples++;

      // calculate LOCS and vLocs and add to list
      locp = ((double) totalFails / totalSamples);
      vLocp = (1.0 / totalSamples) * locp * (1 - locp);

      locsList.add(locp);
      vLocsList.add(vLocp);

      if (locp == 0)
        sigma = 1.0;
      else
        sigma = (Math.sqrt(vLocp) / locp);

      if ((totalSamples > 10 && sigma < 0.08)
              || (totalSamples >= 20000 && locp < 0.0001 && sigma == 1)
              || (totalSamples >= 30000 && sigma < 0.35))
        break;
    }

    SimulationResult sr =
            new SimulationResult(0, 0, 0, 0, totalFails, totalSamples, locp, sigma,
                    true, true, 0, pNeeded, mNeeded, hNeeded, bNeeded, ssd.getCounts(),
                    cpuAvail, memAvail, hddAvail, bandAvail, locsList, vLocsList);

    Connection conn = null;
    PreparedStatement ps = null;
    String insert =
            "INSERT into state (sim_id, sim_res_id, sample, cpu_avail, mem_avail, hdd_avail, ban_avail, rel, var_rel) "
                    + "VALUES (?,?,?,?,?,?,?,?,?);";


    try {
      conn = DB.getConnection();
      conn.setAutoCommit(false);
      int batchLimit = 1000;
      ps = conn.prepareStatement(insert);

      String sim_id = ssd.getSim().getId().toString();
      String sim_res_id = sr.getId().toString();

      try {
        for (int i = 0; i < cpuAvail.size(); i++) {
          ps.setString(1, sim_id);
          ps.setString(2, sim_res_id);
          ps.setInt(3, i);
          ps.setLong(4, cpuAvail.get(i));
          ps.setLong(5, memAvail.get(i));
          ps.setLong(6, hddAvail.get(i));
          ps.setLong(7, bandAvail.get(i));
          ps.setDouble(8, locsList.get(i));
          ps.setDouble(9, vLocsList.get(i));
          ps.addBatch();

          batchLimit--;
          if (batchLimit == 0) {
            ps.executeBatch();
            ps.clearBatch();
            batchLimit = 1000;
          }

          ps.clearParameters();
        }
      } finally {
        ps.executeBatch();
        conn.commit();
      }
    } catch (SQLException e) {
      e.printStackTrace();
      conn.rollback();
    } finally {
      if (ps != null)
        ps.close();

      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }

    return new ReliabilityCurveResultData(sim.getNumVMs(), 1 - locp);
  }
}
