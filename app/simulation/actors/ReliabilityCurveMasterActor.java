package simulation.actors;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinRouter;
import controllers.Application;
import models.Simulation;
import models.SimulationResult;
import simulation.SimUtils;
import simulation.data.AllocationData;
import simulation.data.ReliabilityCurveResultData;
import simulation.data.SimSetupData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/19/13 Time: 8:11 PM
 */
public class ReliabilityCurveMasterActor extends UntypedActor {
  private Simulation sim;
  private final AtomicInteger numRet;
  private final double[] availArr;
  private final int numSims;

  private int numVMs;
  private double alpha = 0.03;
  private long startTime;
  private static final double TOLERANCE = 0.01;

  private final ArrayList<Integer> numVmList = new ArrayList<>();
  private final ArrayList<Double> availList = new ArrayList<>();
  private final ArrayList<Double> stdevList = new ArrayList<>();

  private ActorRef simSetupActor;
  private ActorRef availWorkerActor;

  private boolean aboveZero = false;

  public ReliabilityCurveMasterActor(String simId) {
    this.sim = Simulation.findById(simId);
    this.numSims = sim.getNumSims();
    System.out.println("Started AvailSimMaster: " + sim.getId());

    simSetupActor = getContext().actorOf(
            new Props(SimSetupActor.class).withRouter(new RoundRobinRouter(2)),
            "setup");

//    availWorkerActor =
//        getContext().actorOf(new Props(ReliabilityCurveWorkerActor.class)
//            .withRouter(new RoundRobinRouter(numSims)), "worker");
    availWorkerActor =
            getContext().actorOf(new Props(ReliabilityCurveWorkerActor.class)
                    .withRouter(new RoundRobinRouter(4)), "worker");

    numRet = new AtomicInteger(0);

    this.numVMs = SimUtils.calculateVMsForFail(sim);
    sim.setNumVMs(numVMs);

    availArr = new double[sim.getNumSims()];
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof String) {
      startTime = System.nanoTime();

      for (int i = 0; i < numSims; i++) {
        simSetupActor.tell(new AllocationData(sim), getSelf());
      }
    } else if (message instanceof SimSetupData) {
      availWorkerActor.tell(message, getSelf());
    } else if (message instanceof ReliabilityCurveResultData) { // AvailSimWorker
      // returning a
      // result
      if (numRet.getAndIncrement() < numSims) {
        availArr[numRet.get() - 1] =
                ((ReliabilityCurveResultData) message).getAvailability();
        System.out.println(availArr[numRet.get() - 1]);
      }

      if (numRet.get() == numSims) {
        double avgAvail = SimUtils.average(availArr);
        double stdev = 0;

        if (avgAvail > 0 && numSims > 1)
          stdev = SimUtils.stddev(availArr, avgAvail);

        System.out.println("NumVMs: " + numVMs + " Avg Rel.: " + avgAvail
                + " Std. Dev: " + stdev);

        if (avgAvail > 0) {
          numVmList.add(numVMs);
          availList.add(avgAvail);
          stdevList.add(stdev);
        }

        if (sim.getConvergenceType().equals("Full")) {
          if (avgAvail >= sim.getDesiredAvail() && aboveZero) {
            exitSim();
          } else {
            if (avgAvail >= 0.99 && aboveZero)
              numVMs -= 10;
            else if (avgAvail >= 0.965 && aboveZero)
              numVMs -= 5;
            else if (avgAvail == 0 && !aboveZero)
              numVMs -= 100;
            else {
              if (!aboveZero)
                numVMs += 100;

              numVMs--;
              aboveZero = true;
            }

            sim.setNumVMs(numVMs);
            numRet.set(0);
            for (int i = 0; i < numSims; i++) {
              simSetupActor.tell(new AllocationData(sim), getSelf());
            }
          }
        } else if (sim.getConvergenceType().equals("Quick")) {
          if (!(Math.abs(avgAvail - sim.getDesiredAvail()) > TOLERANCE)) {
            exitSim();
          } else {
            if (avgAvail <= sim.getDesiredAvail())
              numVMs = numVMs - (int) (numVMs * alpha);
            else // avg > desiredAvail
              numVMs = numVMs + (int) (numVMs * alpha);

            alpha *= 0.8;

            System.out.println("New NumVMs: " + numVMs);

            sim.setNumVMs(numVMs);
            numRet.set(0);
            for (int i = 0; i < numSims; i++) {
              simSetupActor.tell(new AllocationData(sim), getSelf());
            }
          }
        }
      }
    } else {
      unhandled(message);
    }
  }

  private void exitSim() {
    System.out.println("Reliability goal reached - all " + sim.getNumSims()
            + " sims had avg. reliability > " + sim.getDesiredAvail());
    cleanupAndSaveResults();

    // tell parent to poison actor tree
    this.context().parent().tell("kill", getSelf());
  }

  public void cleanupAndSaveResults() {
    long duration = System.nanoTime() - startTime;
    double runTime = (duration / 1000000.0) / 1000.0; // in seconds

    SimulationResult sr = new SimulationResult(true, true, runTime, numVmList,
            availList, stdevList);
    Simulation.addResult(sim, sr, runTime, 0, 0);

    Application.pushSimNotification(sim.getId().toString());
  }

  private static boolean allAboveDesiredAvail(double[] avail,
                                              double desiredAvail) {
    for (double anAvail : avail) {
      if (anAvail < desiredAvail)
        return false;
    }
    return true;
  }
}
