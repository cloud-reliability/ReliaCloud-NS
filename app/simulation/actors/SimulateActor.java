package simulation.actors;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Allocation.VmAllocation;
import akka.actor.UntypedActor;
import models.Cluster;
import models.Simulation;
import models.SimulationResult;
import play.db.DB;
import simulation.SimUtils;
import simulation.data.ResultData;
import simulation.data.SimSetupData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/14/13 Time: 11:19 PM
 */
class SimulateActor extends UntypedActor {
  boolean downsample = true;
  final int NUMBER_OF_PTS = 300;
  private static final boolean printFails = false;

  Connection conn = null;
  PreparedStatement ps = null;

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof SimSetupData) {
      sender().tell(chooseSimulationMethod((SimSetupData) message), getSelf());
    } else {
      unhandled(message);
    }
  }

  private ResultData chooseSimulationMethod(SimSetupData ssd){

    ResultData rd = null;
    SimulationResult sr = null;
    try{
      if (ssd.getVmAllocationType().equals("Original"))  { sr = VmAllocation.original(ssd); }
      else if (ssd.getVmAllocationType().equals("FCFS")) { sr = VmAllocation.firstComeFirstServe(ssd); }

      rd = saveResults(ssd, sr);
    }
    catch(Exception e) {
        System.out.println(e.toString());
    }

    return rd;

  }


  private ResultData saveResults(SimSetupData ssd, SimulationResult sr) throws SQLException{
    conn = null;
    ps = null;
    String insert =
            "INSERT into state (sim_id, sim_res_id, sample, cpu_avail, mem_avail, hdd_avail, ban_avail, rel, var_rel) " // column names
                    + "VALUES (?,?,?,?,?,?,?,?,?);";


    try {
      conn = DB.getConnection();
      conn.setAutoCommit(false);   // method on sql must set false
      int batchLimit = 1000;   // number of request available
      ps = conn.prepareStatement(insert);   // to insert row in database

      String sim_id = ssd.getSim().getId().toString();
      String sim_res_id = sr.getId().toString();

      try {
        for (int i = 0; i < sr.getLocpList().size(); i++) {
          ps.setString(1, sim_id);
          ps.setString(2, sim_res_id);
          ps.setInt(3, i);
          ps.setLong(4, 0);
          ps.setLong(5, 0);
          ps.setLong(6, 0);
          ps.setLong(7,0);
          ps.setDouble(8, sr.getLocpList().get(i));
          ps.setDouble(9, sr.getvLocpList().get(i));
          ps.addBatch();

          batchLimit--;
          if (batchLimit == 0) {
            ps.executeBatch();
            ps.clearBatch();
            batchLimit = 1000;
          }

          ps.clearParameters();
        }
      } finally {
        ps.executeBatch();
        conn.commit();
      }
    } catch (SQLException e) {
      e.printStackTrace();
      conn.rollback();
    } finally {
      if (ps != null)
        ps.close();

      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }

    conn = null;
    ps = null;

    boolean saveResult = false;
    boolean saveResources = true;

    if (ssd.getSim().getVmDistributionType().equals("Identical")) {
      saveResult = true;
    }
    return new ResultData(sr, saveResult, saveResources);
  }

}

