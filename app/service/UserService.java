package service;

/**
 * Copyright 2012 Jorge Aliss (jaliss at gmail dot com) - twitter: @jaliss
 *
 * Code modified by Brett Snyder - 2013
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
import models.User;
import play.Application;
import securesocial.core.Identity;
import securesocial.core.IdentityId;
import securesocial.core.OAuth2Info;
import securesocial.core.SocialUser;
import securesocial.core.java.BaseUserService;
import securesocial.core.java.Token;

public class UserService extends BaseUserService {
  public UserService(Application application) {
    super(application);
  }

  @Override
  public Identity doSave(Identity user) {
    boolean alreadyInDB = false;
    // check to see if the user is already persisted in DB

    try {
      SocialUser s = User.findByUserId(user.identityId().userId());
      if (s != null) {
        alreadyInDB = true;
        System.out.println(user.identityId().userId() + ": " + user.fullName()
            + " is already saved in DB.");
        return s;
      }
    } catch (final NullPointerException np) {
      System.out.println(
          "doSave: Caught null pointer exception while trying to find user in database.");
    }

    if (!alreadyInDB) {
      User u = new User();
      u.setUserId(user.identityId().userId());
      u.setProvider(user.identityId().providerId());
      u.setFirst(user.firstName());
      u.setLast(user.lastName());

      if (user.email().isDefined())
        u.setEmail(user.email().get());

      u.setAuthMethod(user.authMethod().method());

      if (user.oAuth1Info().isDefined()) {
        u.setToken(user.oAuth1Info().get().token());
        u.setSecret(user.oAuth1Info().get().secret());
      }

      if (user.oAuth2Info().isDefined()) {
        OAuth2Info oa2 = user.oAuth2Info().get();
        u.setAccessToken(oa2.accessToken());
        if (oa2.tokenType().isDefined())
          u.setTokenType(oa2.tokenType().get());
        if (oa2.refreshToken().isDefined())
          u.setRefreshToken(oa2.refreshToken().get());
      }

      if (user.passwordInfo().isDefined()) {
        u.setHasher(user.passwordInfo().get().hasher());
        u.setPassword(user.passwordInfo().get().password());
        if (user.passwordInfo().get().salt().isDefined())
          u.setSalt(user.passwordInfo().get().salt().get());
      }

      u.setActive(true);
      User.create(u);
      return User.findByUserId(u.getUserId());
    }
    // else
    return null;
  }

  @Override
  public void doSave(Token token) {}

  @Override
  public Identity doFind(IdentityId userId) {
    try {
      return User.findByUserId(userId.userId());
    } catch (final NullPointerException np) {
      System.out.println(
          "doFind: Caught null pointer exception while trying to find user in database.");
    }
    return null;
  }

  @Override
  public Token doFindToken(String tokenId) {
    return null;
  }

  @Override
  public Identity doFindByEmailAndProvider(String email, String providerId) {
    return null;
  }

  @Override
  public void doDeleteToken(String uuid) {}

  @Override
  public void doDeleteExpiredTokens() {}
}
