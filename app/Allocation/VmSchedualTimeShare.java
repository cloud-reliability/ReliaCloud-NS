package Allocation;

import models.Cluster;
import models.VM;

import java.util.List;
import java.util.Map;


// allocate cluster's nodes for Vms runs in that cluster
// we have number of nodes of specific cluster .. in scheduling method we allcate nodes according to vm needs

public class VmSchedualTimeShare {


    private int cores;
    private int avaliCores;
    private int allocatedCores;
    private Map<String,Integer> coresTable;

    private int memory;
    private int avaliMemory;
    private int allocatedMem;
    private Map<String,Integer> memTable;


    private int bw;
    private int avaliBw;
    private int allocatedBw;
    private Map<String,Integer> bwTable;


    private int hdd;
    private int avaliHdd;
    private int allocatedHdd;
    private Map<String,Integer> hddTable;


    /**************************************************************************/

    public VmSchedualTimeShare(){
        setAvaliCores(0);
        setAvaliMemory(0);
        setAvaliBw(0);
        setAvaliHdd(0);
    }
    public VmSchedualTimeShare(Cluster c){

        c.setCores(c.getCores()*c.getNodes());
        c.setMemory(c.getMemory()*c.getNodes());
        c.setBandwidth(c.getBandwidth()*c.getNodes());
        c.setHdd(c.getHdd()*c.getNodes());

        setAvaliCores(c.getCores());
        setAvaliMemory(c.getMemory());
        setAvaliBw(c.getBandwidth());
        setAvaliHdd(c.getHdd());


    }


    public boolean allocateCoresToVm(VM vm) {

        deallocateCoresForVm(vm);

        String vm_id = vm.getId().toString();

        // not allow oversubscription
        if ( getAvaliCores() < vm.getCores()) {
            setAllocatedCores(getAllocatedCoresForVm(vm));
            return false;

        }

        setAvaliCores(getAvaliCores()- vm.getCores());
        getCoresTable().put(vm_id,vm.getCores());
        setAllocatedCores(getAllocatedCoresForVm(vm));

        return true;

    }

    public boolean allocateBWToVm(VM vm) {

        deallocateBwForVm(vm);

        String vm_id = vm.getId().toString();


        // not allow oversubscription
        if ( getAvaliBw()< vm.getBandwidth()) {
            setAllocatedBw(getAllocatedBwForVm(vm));
            return false;

        }

        setAvaliBw(getAvaliBw()- vm.getBandwidth());
        getBwTable().put(vm_id,vm.getBandwidth());
        setAllocatedBw(getAllocatedBwForVm(vm));

        return true;

    }


    public boolean allocateHddToVm(VM vm) {

        deallocateHddForVm(vm);

        String vm_id = vm.getId().toString();

        // not allow oversubscription
        if ( getAvaliHdd()< vm.getHdd()) {
            setAllocatedHdd(getAllocatedHddForVm(vm));
            return false;

        }

        setAvaliHdd(getAvaliHdd()- vm.getHdd());
        getHddTable().put(vm_id,vm.getHdd());
        setAllocatedHdd(getAllocatedHddForVm(vm));
        return true;

    }



    public boolean allocateMemoryToVm(VM vm) {

        deallocateMemForVm(vm);

        String vm_id = vm.getId().toString();

        // not allow oversubscription
        if ( getAvaliMemory()< vm.getMemory()) {
            setAllocatedMem(getAllocatedMemForVm(vm));
            return false;

        }

        setAvaliMemory(getAvaliMemory()- vm.getMemory());
        getMemTable().put(vm_id,vm.getMemory());
        setAllocatedMem(getAllocatedMemForVm(vm));
        return true;

    }






    public int getAllocatedBwForVm(VM vm) {

        if (getBwTable().containsKey(vm.getId()))
            return getBwTable().get(vm.getId());

        return 0;
    }

    public int getAllocatedMemForVm(VM vm) {

        if (getMemTable().containsKey(vm.getId()))
            return getMemTable().get(vm.getId());

        return 0;
    }


    public int getAllocatedCoresForVm(VM vm) {

        if (getCoresTable().containsKey(vm.getId()))
            return getCoresTable().get(vm.getId());

        return 0;
    }

    public int getAllocatedHddForVm(VM vm) {

        if (getHddTable().containsKey(vm.getId()))
            return getHddTable().get(vm.getId());

        return 0;
    }



    public void deallocateBwForVm(VM vm) {

        if (getBwTable().containsKey(vm.getId())) {
            int removedNumber = getBwTable().remove(vm.getId());
            setAvaliBw(getAvaliBw() + removedNumber);
            vm.setAllocatedBW(0);
        }
    }


    public void deallocateMemForVm(VM vm) {

        if (getMemTable().containsKey(vm.getId())) {
            int removedNumber = getMemTable().remove(vm.getId());
            setAvaliMemory(getAvaliMemory() + removedNumber);
            vm.setAllocatedMem(0);
        }
    }


    public void deallocateHddForVm(VM vm) {

        if (getHddTable().containsKey(vm.getId())) {
            int removedNumber = getHddTable().remove(vm.getId());
            setAvaliHdd(getAvaliHdd() + removedNumber);
            vm.setAllocatedHdd(0);
        }
    }


    public void deallocateCoresForVm(VM vm) {

        if (getCoresTable().containsKey(vm.getId())) {
            int removedNumber = getCoresTable().remove(vm.getId());
            setAvaliCores(getAvaliCores() + removedNumber);
            vm.setAllocatedCores(0);
        }
    }







    /***************************** set & get Methods **********************************/

    public Map<String, Integer> getHddTable() {
        return hddTable;
    }

    public void setHddTable(Map<String, Integer> hddTable) {
        this.hddTable = hddTable;
    }

    public Map<String, Integer> getBwTable() {
        return bwTable;
    }

    public void setBwTable(Map<String, Integer> bwTable) {
        this.bwTable = bwTable;
    }

    public Map<String, Integer> getMemTable() {
        return memTable;
    }

    public void setMemTable(Map<String, Integer> memTable) {
        this.memTable = memTable;
    }

    public Map<String, Integer> getCoresTable() {
        return coresTable;
    }

    public void setCoresTable(Map<String, Integer> coresTable) {
        this.coresTable = coresTable;
    }

    public int getAllocatedCores() {
        return allocatedCores;
    }

    public void setAllocatedCores(int allocatedCores) {
        this.allocatedCores = allocatedCores;
    }

    public int getAllocatedMem() {
        return allocatedMem;
    }

    public void setAllocatedMem(int allocatedMem) {
        this.allocatedMem = allocatedMem;
    }

    public int getAllocatedBw() {
        return allocatedBw;
    }

    public void setAllocatedBw(int allocatedBw) {
        this.allocatedBw = allocatedBw;
    }

    public int getAllocatedHdd() {
        return allocatedHdd;
    }

    public void setAllocatedHdd(int allocatedHdd) {
        this.allocatedHdd = allocatedHdd;
    }

    public int getAvaliCores() {
        return avaliCores;
    }

    public void setAvaliCores(int avaliCores) {
        this.avaliCores = avaliCores;
    }

    public int getAvaliMemory() {
        return avaliMemory;
    }

    public void setAvaliMemory(int avaliMemory) {
        this.avaliMemory = avaliMemory;
    }

    public int getAvaliBw() {
        return avaliBw;
    }

    public void setAvaliBw(int avaliBw) {
        this.avaliBw = avaliBw;
    }

    public int getAvaliHdd() {
        return avaliHdd;
    }

    public void setAvaliHdd(int avaliHdd) {
        this.avaliHdd = avaliHdd;
    }

    public int getHdd() {
        return hdd;
    }

    public void setHdd(int hdd) {
        this.hdd = hdd;
    }

    public int getBw() {
        return bw;
    }

    public void setBw(int bw) {
        this.bw = bw;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }
}
