$(function() {
    $('td:last-child input').change(function() {
        $(this).closest('tr').toggleClass("success", this.checked)
    });
});
