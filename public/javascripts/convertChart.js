/**
 * Created with IntelliJ IDEA.
 * User: Brett
 * Date: 4/3/13
 * Time: 12:31 PM
 */

function getImgData(chartContainer) {
    var chartArea = chartContainer.getElementsByTagName('svg')[0].parentNode;
    var svg = chartArea.innerHTML;
    var doc = chartContainer.ownerDocument;
    var canvas = doc.createElement('canvas');
    canvas.setAttribute('width', chartArea.offsetWidth);
    canvas.setAttribute('height', chartArea.offsetHeight);

    canvas.setAttribute(
        'style',
        'position: absolute; ' +
            'top: ' + (-chartArea.offsetHeight * 2) + 'px;' +
            'left: ' + (-chartArea.offsetWidth * 2) + 'px;');
    doc.body.appendChild(canvas);
    canvg(canvas, svg);
    var imgData = canvas.toDataURL('image/png');
    canvas.parentNode.removeChild(canvas);
    return imgData;
}

function toImg(chartContainer, imgContainer) {
    var doc = chartContainer.ownerDocument;
    var img = doc.createElement('img');
    img.src = getImgData(chartContainer);

    while (imgContainer.firstChild) {
        imgContainer.removeChild(imgContainer.firstChild);
    }
    imgContainer.appendChild(img);
}

function convertNormal(){
    toImg(document.getElementById('converge_div'), document.getElementById('converge_div'));
    toImg(document.getElementById('var_div'), document.getElementById('var_div'));
    toImg(document.getElementById('compfail_div'), document.getElementById('compfail_div'));
    toImg(document.getElementById('dist_div'), document.getElementById('dist_div'));
    toImg(document.getElementById('cpuChart_div'), document.getElementById('cpuChart_div'));
    toImg(document.getElementById('memChart_div'), document.getElementById('memChart_div'));
    toImg(document.getElementById('hddChart_div'), document.getElementById('hddChart_div'));
    toImg(document.getElementById('banChart_div'), document.getElementById('banChart_div'));
}
