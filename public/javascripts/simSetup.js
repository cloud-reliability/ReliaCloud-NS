$(function(){
    //dont allow enter to submit form until prob distribution holds
    document.onkeypress = stopRKey;
    var vmgroup     = $("#vmgroup");
    var vmgroupid   = vmgroup.val();
    var cloud       = $("#cloudid");
    var cloudid     = cloud.val();
    var vTable      = $("#vmTable");
    var cTable      = $("#cloudTable");
    var oTable      = $("#overviewTable");
    var numVMBox    = $("#numVMs");
    var numVMs      = numVMBox.val();
    var numVMDiv    = $("#numVMDiv");
    var availRadio  = $("#simType_Reliability");
    var normalSim   = $("#normalSim");
    var baseVMs     = $("#baseVMs");
    var availSim    = $("#availSim");
    var factorRadio = $("#oversubType_Oversubscription");
    var nonRadio    = $("#oversubType_NormalType");
    var factorTxt   = $("#factorTxt");
    var desAvail    = $("#desAvail");



    cTable.load('/sim/tables?vmgroupid='+vmgroupid+'&cloudid='+cloudid+' #cloudlist');

    vmgroup.change(function(){
        vmgroupid = $(this).val();
        checkNumVMs();

        vTable.load('/sim/tables?vmgroupid='+vmgroupid+'&cloudid='+cloudid+' #vmlist', function(){
            oTable.load('/sim/tables/over?vmgroupid='+vmgroupid+'&cloudid='+cloudid+genProbUrl()+'&numVMs='+numVMs+' #overview');
        }); //load updates when selection changes

        $("button").addClass("disabled btn-danger").removeClass("btn-success");
    });

    cloud.change(function(){
        cloudid = $(this).val();
        checkNumVMs();

        cTable.load('/sim/tables?vmgroupid='+vmgroupid+'&cloudid='+cloudid+' #cloudlist', function(){
            oTable.load('/sim/tables/over?vmgroupid='+vmgroupid+'&cloudid='+cloudid+genProbUrl()+'&numVMs='+numVMs+' #overview');
        }); //load updates when selection changes
    });

    $(document).on("keyup",numVMBox, function(){
        numVMs = numVMBox.val();
        checkNumVMs();

        oTable.load('/sim/tables/over?vmgroupid='+vmgroupid+'&cloudid='+cloudid+genProbUrl()+'&numVMs='+numVMs+' #overview');
    });

    $(document).on("keyup",".prob", function(){
        updateProbabilityTable();
        checkNumVMs();

        oTable.load('/sim/tables/over?vmgroupid='+vmgroupid+'&cloudid='+cloudid+genProbUrl()+'&numVMs='+numVMs+' #overview');
    });

    function changeEnabling(item1, item2){
        if(item1.prop('disabled') == false){
            item1.prop('disabled', true);
        }else{
            item1.prop('disabled', false);
        }
        if(item2.prop('disabled') == false){
            item2.prop('disabled', true);
        }else{
            item2.prop('disabled', false);
        }
    }
    $("#simType_Normal").change(function(){
        changeEnabling(numVMBox, desAvail);
    });
    $("#simType_Reliability").change(function(){
        changeEnabling(numVMBox, desAvail);
    });

    availRadio.change(function(){
        availSim.show(200);
        normalSim.hide(200);
    });

    if(availRadio.attr("checked") != "undefined" && availRadio.attr("checked") == "checked"){
        availSim.show(200);
        normalSim.hide(200);
    }

    if(factorRadio.attr("checked") != "undefined" ){
        factorTxt.hide(200);
    }
    nonRadio.change(function(){
        factorTxt.hide(200);
    });

    factorRadio.change(function(){
        factorTxt.show(200);
    });



    baseVMs.click(function(){
        numVMBox.val("100");   //need to use calculate VMs from failure
        numVMBox.load('/sim/calc?cloudid='+cloudid+'&');
        numVMs = numVMBox.val();
        checkNumVMs();

        oTable.load('/sim/tables/over?vmgroupid='+vmgroupid+'&cloudid='+cloudid+genProbUrl()+'&numVMs='+numVMs+' #overview');
    });

    function checkNumVMs(){
        if(isNaN(numVMs) || numVMs == "")
            numVMs = 0;
    }
});

function genProbUrl(){
    var probArr = [];
    probArr[0]="";  //so that an '&' is bound at beginning
    var i=1;
    $(".prob").each(function(){
        var val = $(this).val();
        if (isNaN(val) || val == "" || val == "0.0" || val == "0" || val == "0."){
        }else{
            probArr[i] = this.id + "=" + val;
            i++;
        }
    });
    return probArr.join('&')
}

function sumProbabilities(){
    var sum = 0;
    $(".prob").each(function(){
        var val = $(this).val();
        if (isNaN(val) || val == ""){
            $(this).val("0.");
        }else if(val == "0.0"){
            //do nothing
        }
        else{
            sum += parseFloat(val);
        }
    });
    return sum;
}

function updateProbabilityTable(){
    var sum = sumProbabilities();
    $("#probTotal").text(sum);

    var row = $("#probTotalRow");
    var button = $("button");
    var baseVMs = $("#baseVMs");
    if(sum == 1.0){
        row.removeClass("error").addClass("success");
        button.removeClass("disabled btn-danger").addClass("btn-success").removeAttr("disabled");
        baseVMs.removeClass("disabled btn-danger").addClass("btn-success").removeAttr("disabled");
    }else{
        row.removeClass("success").addClass("error");
        button.addClass("disabled btn-danger").removeClass("btn-success").attr("disabled","disabled");
        baseVMs.addClass("disabled btn-danger").removeClass("btn-success").attr("disabled","disabled");
    }
}

function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}
