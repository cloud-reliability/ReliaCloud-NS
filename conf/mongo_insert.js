db.VM.remove();
db.Cluster.remove();
db.VMGroup.remove();
db.Cloud.remove();

db.VM.insert({
  name: "M1 Small",
  cores: 2,
  memory: 2,
  hdd: 160,
  bandwidth: 100,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "M1 Medium",
  cores: 4,
  memory: 4,
  hdd: 410,
  bandwidth: 500,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "M1 Large",
  cores: 8,
  memory: 8,
  hdd: 850,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "M1 Extra Large",
  cores: 16,
  memory: 16,
  hdd: 1690,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "M3 Extra Large",
  cores: 26,
  memory: 16,
  hdd: 1690,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "M3 Double Extra Large",
  cores: 52,
  memory: 32,
  hdd: 3380,
  bandwidth: 2000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "High Memory Extra Large",
  cores: 13,
  memory: 18,
  hdd: 420,
  bandwidth: 500,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "High Memory Double Extra Large",
  cores: 26,
  memory: 35,
  hdd: 850,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "High Memory Quad Extra Large",
  cores: 52,
  memory: 70,
  hdd: 1690,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "High CPU Medium",
  cores: 10,
  memory: 2,
  hdd: 350,
  bandwidth: 500,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "High CPU Extra Large",
  cores: 40,
  memory: 8,
  hdd: 1690,
  bandwidth: 1000,
  isActive: true,
  isDefault: true
});
db.VM.insert({
  name: "Cluster Eight Extra Large",
  cores: 176,
  memory: 64,
  hdd: 3370,
  bandwidth: 10000,
  isActive: true,
  isDefault: true
});

db.Cluster.insert({
  name: "Ranger",
  cores: 16,
  memory: 32,
  hdd: 460,
  bandwidth: 100,
  nodes: 3936,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Wispy",
  cores: 4,
  memory: 16,
  hdd: 250,
  bandwidth: 1000,
  nodes: 32,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "GordonION",
  cores: 12,
  memory: 48,
  hdd: 4000,
  bandwidth: 100000,
  nodes: 64,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "KrakenXT75",
  cores: 12,
  memory: 16,
  hdd: 261,
  bandwidth: 1000,
  nodes: 9408,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Lonestar4",
  cores: 12,
  memory: 24,
  hdd: 146,
  bandwidth: 10000,
  nodes: 1888,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Steele",
  cores: 8,
  memory: 32,
  hdd: 500,
  bandwidth: 1000,
  nodes: 893,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Trestles",
  cores: 32,
  memory: 64,
  hdd: 442,
  bandwidth: 10000,
  nodes: 324,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Quarry",
  cores: 8,
  memory: 32,
  hdd: 19,
  bandwidth: 1000,
  nodes: 112,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Stampede",
  cores: 16,
  memory: 32,
  hdd: 50,
  bandwidth: 13640,
  nodes: 6400,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Blacklight",
  cores: 512,
  memory: 16384,
  hdd: 75000,
  bandwidth: 7500,
  nodes: 2,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Keeneland",
  cores: 16,
  memory: 32,
  hdd: 6671,
  bandwidth: 13640,
  nodes: 264,
  isActive: true,
  isDefault: true
});
db.Cluster.insert({
  name: "Gordon Compute Cluster",
  cores: 16,
  memory: 64,
  hdd: 4000,
  bandwidth: 100000,
  nodes: 1024,
  isActive: true,
  isDefault: true
});
